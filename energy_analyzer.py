import numpy as np

#Establish example lists that we will test with
#Home Appliances
appliances = [0.2, 1.5, 5.0]
#Truth values
truth = [0.2, 5.0, 1.5]
#reading value
reading = [0.19, 1.12, 1.46]

'''
Find the percentage difference by taking
the reading array and dividing it by the truth array,
then create an appliance dictionary and two temporary lists.
The two temp lists will allow us to iterate over the appliance dictionary
and remove values from the temp lists when criteria are met.
'''

percent_diff = np.divide(reading, truth)
applianceDict = dict(zip(reading, percent_diff))
ctruth = dict(zip(reading, truth))
ad = dict(applianceDict)

#empty lists
true_p = []
false_p = []
edge_v = []
#anything left in truth that is not placed somewhere else goes into false n
#missed_v = list(set(appliances) - set(ctruth))
false_n = []

for key, value in applianceDict.iteritems():
    if value >= .90:
        true_p += (key, value),
        del ad[key]
        del ctruth[key]
    elif .87 <= value <= .93:
        edge_v += (key, value),
    elif value < .90:
        false_p += (key, value),
        del ad[key]

false_n.append(ctruth.values())

print 'Found ' + str(len(true_p)) + ' true positives.'
print 'Found ' + str(len(false_p)) + ' false positives.'
print 'Found ' + str(len(false_n)) + ' false negatives.'
print 'Found ' + str(len(edge_v)) + ' edge values.'

#For debugging purposes
'''
print percent_diff
print true_p
print false_p
print false_n
print edge_v
print applianceDict
print ad
print ctruth.values()
'''